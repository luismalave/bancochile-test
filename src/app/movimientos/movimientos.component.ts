import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movimientos',
  templateUrl: './movimientos.component.html',
  styleUrls: ['./movimientos.component.sass']
})
export class MovimientosComponent implements OnInit {
  movimientos = [
    {
      numero: 1,
      fecha: '01 / 01 / 2019',
      cuenta: '0001 1234 1292',
      destino: '0291 0937 4991',
      asunto: 'Compra McDonalds Ahumada...',
      monto: '$ 7300'
    },
    {
      numero: 2,
      fecha: '01 / 08 / 2019',
      cuenta: '0001 1234 1292',
      destino: '0101 2221 6632',
      asunto: 'Asado de cumple',
      monto: '$ 12890'
    },
    {
      numero: 3,
      fecha: '01 / 10 / 2019',
      cuenta: '0001 1234 1292',
      destino: '6582 1919 3381',
      asunto: 'Gastos comunes',
      monto: '$ 80000'
    },
    {
      numero: 4,
      fecha: '01 / 23 / 2019',
      cuenta: '0001 1234 1292',
      destino: '1010 2291 6522',
      asunto: 'Viaje a chiloe de fin de sem...',
      monto: '$ 65800'
    },
    {
      numero: 5,
      fecha: '01 / 01 / 2019',
      cuenta: '0001 1234 1292',
      destino: '0291 0937 4991',
      asunto: 'Compra McDonalds Ahumada...',
      monto: '$ 7300'
    },
  ]
  constructor() {}

  ngOnInit() {
  }

}
